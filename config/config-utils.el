;;
;; Util Functions
;;

;; 
;; Duplizieren der aktuellen Zeile und binden an 
;; die Tastenkombination C-c C-d
;;
(defun sra/duplicate-line() 
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "C-c C-d") 'sra/duplicate-line)

(defun sra/smart-open-line-above()
  "Insert an empty line above the current line and smart indents
accordingly to the current buffer major-mode."
  (interactive)
  (move-beginning-of-line nil)
  (newline-and-indent)
  (forward-line -1)
  (indent-according-to-mode))

(global-set-key [(control shift return)] 'sra/smart-open-line-above)

(provide 'config-utils)
