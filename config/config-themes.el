;;
;; Theme und Font Config
;;

;; Font Face setzen
(set-face-font 'default (font-spec :family "Consolas" :size 15))

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'zenburn t)

(provide 'config-themes)
