;;
;; cider Mode
;;

(add-to-list 'load-path "~/.emacs.d/plugins/cider")

(require 'cider)
