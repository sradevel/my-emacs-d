;;
;; org-mode Konfiguration
;;

(add-to-list 'load-path "~/.emacs.d/plugins/org-mode/lisp")
(add-to-list 'load-path "~/.emacs.d/plugins/org-mode/contrib")

;; Dateien, die für die Agenda Views genutzt werden sollen
(setq org-agenda-files (list "~/org/index.org"
                             "~/org/notes.org"
                             ))

;; Alle Org-Mode Dateien gehen hier rein
(setq org-directory "~/org")

;; MobileOrg Inbox
(setq org-mobile-inbox-for-pull "~/org/flagged.org")

;; Dateien die mit MobileOrg synchronisiert werden sollen
(setq org-mobile-files (list "~/org/index.org"
                             "~/org/notes.org"))

;; Clean View
(setq org-hide-leading-stars t)

;; Verzeichnis indem die Dateien für MobileOrg generiert werden
(setq org-mobile-directory "~/Dropbox/org")

;; electric-indent in Org Buffer verhindern
;; Quelle: http://foldl.me/2012/disabling-electric-indent-mode/
(add-hook 'org-mode-hook
          (lambda ()
            (set (make-local-variable 'electric-indent-functions)
                 (list (lambda (arg) 'no-indent)))))
