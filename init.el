;; 
;; Emacs Konfiguration
;;

(add-to-list 'load-path "~/.emacs.d/config")

(require 'config-basic)    ;; lade Basis Konfiguration
(require 'config-themes)   ;; lade Themes und Font Config
(require 'config-modes)    ;; lade Modes und Plugins
(require 'config-utils)    ;; lade Utility Functions
