;;; puppet-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (puppet-mode) "puppet-mode" "puppet-mode.el" (21569
;;;;;;  34294 246526 737000))
;;; Generated autoloads from puppet-mode.el

(autoload 'puppet-mode "puppet-mode" "\


\(fn)" t nil)

(add-to-list 'auto-mode-alist '("\\.pp\\'" . puppet-mode))

;;;***

;;;### (autoloads nil nil ("puppet-mode-pkg.el") (21569 34294 271342
;;;;;;  60000))

;;;***

(provide 'puppet-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; puppet-mode-autoloads.el ends here
